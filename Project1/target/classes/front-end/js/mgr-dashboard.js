window.onload = () => {    
    console.log('mgr window has been loaded');
    getSessionUser();
    getReimbsByStatus(5);
    document.getElementById('submit').addEventListener('click', submitNewUser);
}

function getReimbsByStatus(statusId) {
    
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = () => {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbs = JSON.parse(xhttp.responseText);
            loadReimbContent(reimbs);
        }
    }

    xhttp.open("GET", `http://localhost:9009/reimbursements/status/${statusId}`);
    xhttp.send();
}

/************************
 * Load Content To Page *
 ************************/

function loadReimbContent(reimbs) {

    let oldTable = document.getElementsByTagName('tbody');
    let newTable = document.createElement('tbody');
    oldTable[0].parentNode.replaceChild(newTable, oldTable[0]);

    for (let reimb of reimbs) {

        let type = getTypeById(reimb.typeId);
        let status = getStatusById(reimb.statusId);
        let table_data = document.createElement('tr');

        if(reimb.statusId == 5) {
            document.getElementById('approve-head').style.visibility = "visible";
            document.getElementById('deny-head').style.visibility = "visible";

            table_data.innerHTML += `<td>${reimb.authorId}</td>`;
            table_data.innerHTML += `<td>$${reimb.amount}</td>`;
            table_data.innerHTML += `<td>${reimb.submitted}</td>`;
            table_data.innerHTML += `<td>${reimb.resolved}</td>`;
            table_data.innerHTML += `<td>${reimb.description}</td>`;
            table_data.innerHTML += `<td>${type}</td>`;
            table_data.innerHTML += `<td id="status_${reimb.statusId}"><strong>${status}</strong></td>`;
            table_data.innerHTML += `<td><button type="button" class="btn btn-success approve" onclick="approveReimb(${reimb.reimbId})">Approve</button></td>`;
            table_data.innerHTML += `<td><button type="button" class="btn btn-danger deny" onclick="denyReimb(${reimb.reimbId})"> Deny  </button></td>`;
        } else {
            document.getElementById('approve-head').style.visibility = "hidden";
            document.getElementById('deny-head').style.visibility = "hidden";

            table_data.innerHTML += `<td>${reimb.authorId}</td>`;
            table_data.innerHTML += `<td>$${reimb.amount}</td>`;
            table_data.innerHTML += `<td>${reimb.submitted}</td>`;
            table_data.innerHTML += `<td>${reimb.resolved}</td>`;
            table_data.innerHTML += `<td>${reimb.description}</td>`;
            table_data.innerHTML += `<td>${type}</td>`;
            table_data.innerHTML += `<td id="status_${reimb.statusId}"><strong>${status}</strong></td>`;
        }
        
        document.querySelector('#reimb-table>tbody').append(table_data);
    }
}

/***************************
 * Reimbursement Functions *
 ***************************/

function approveReimb(reimbId) {
    
    let xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = () => {
        if(xhttp.readyState == 4) {
            if(xhttp.status == 402) {
                alert('It is forbidden to approve or deny your own Reimbursement!');
            } else if(xhttp.status == 222) {
                window.location.href = `/html/mgr-dashboard.html`;
            }
        }
    }

    xhttp.open("POST", `http://localhost:9009/reimbursements/resolve/${reimbId}/approve`);
    xhttp.send();
}

function denyReimb(reimbId) {
    
    let xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = () => {
        if(xhttp.readyState == 4) {
            if(xhttp.status == 402) {
                alert('It is forbidden to approve or deny your own Reimbursement!');
            } else if(xhttp.status == 222) {
                window.location.href = `/html/mgr-dashboard.html`;
            }
        }
    }

    xhttp.open("POST", `http://localhost:9009/reimbursements/resolve/${reimbId}/deny`);
    xhttp.send();
}

/*******************
 * Submit New User *
 *******************/
function submitNewUser() {
  
  let userForm = new FormData();

  userForm.append("firstName", document.getElementById('first-name').value);
  userForm.append("lastName", document.getElementById('last-name').value);
  userForm.append("userName", document.getElementById('username').value);
  userForm.append("email", document.getElementById('email').value);
  userForm.append("role", document.getElementById('role').value);

  let xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = () => {

    if(xhttp.readyState == 4) {

      if(xhttp.status == 200) {
        console.log('User Successfully Created');
      }
    }

  }

  xhttp.open("POST", "http://localhost:9009/createUser");
  xhttp.send(userForm);
}