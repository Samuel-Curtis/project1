window.onload = () => {  
    console.log('user window has been loaded');  
    getSessionUser();
    getUserReimbs();
    document.getElementById('submit').addEventListener('click', submitNewReimb);
}

function getUserReimbs() {
    
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = () => {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbs = JSON.parse(xhttp.responseText);
            
            loadReimbContent(reimbs);
        }
    }

    xhttp.open("GET", "http://localhost:9009/reimbursements")
    xhttp.send();
}

/************************
 * Load Content To Page *
 ************************/

function loadReimbContent(reimbs) {

    for (let reimb of reimbs) {

        let type = getTypeById(reimb.typeId);
        let status = getStatusById(reimb.statusId);
        let table_data = document.createElement('tr');

        table_data.innerHTML += `<td>$${reimb.amount}</td>`;
        table_data.innerHTML += `<td>${reimb.submitted}</td>`;
        table_data.innerHTML += `<td>${reimb.resolved}</td>`;
        table_data.innerHTML += `<td>${reimb.description}</td>`;
        table_data.innerHTML += `<td>${type}</td>`;
        table_data.innerHTML += `<td id="status_${reimb.statusId}"><strong>${status}</strong></td>`;
        
        
        document.querySelector('#reimb-table>tbody').append(table_data);
    }
}

/************************
 * Submit Reimbursement *
 ************************/
function submitNewReimb() {

    let reimbForm = new FormData();

    reimbForm.append("amount", document.getElementById('new-amount').value);
    reimbForm.append("typeId", document.getElementById('inputReason').value)
    reimbForm.append("description", document.getElementById('description').value);

    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = () => {
        if(xhttp.readyState == 4) {
            if(xhttp.status == 222) {
                window.location.href = `/html/user-dashboard.html`;
            } else if(xhttp.status == 440) {
                alert('Please only use numeric values when specifying an amount');
                document.getElementById('submit-fail').style.visibility = 'visible';
            } else if(xhttp.status == 445) {
                alert('Please only use a positive whole number when specifying an amount');
                document.getElementById('submit-fail').style.visibility = 'visible';
            }
            else {
                document.getElementById('submit-fail').style.visibility = 'visible';
            }
        } 
    }

    xhttp.open("POST", "http://localhost:9009/reimbursements/submit");
    xhttp.send(reimbForm);

}