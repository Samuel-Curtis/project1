package com.project1.page.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.project1.page.LoginPage;
import com.project1.page.MgrDashboardPage;
import com.project1.page.UserDashboardPage;

public class MgrDashboardTest {

	private LoginPage loginPage;
	private UserDashboardPage udbPage;
	private MgrDashboardPage mgrPage;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", filePath);
		
		driver = new FirefoxDriver();
		
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.loginPage = new LoginPage(driver);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	/* TESTS */
	
	@Test
	public void testManagerLogin() {
		loginPage.setUsername("sCurtis");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		this.mgrPage = new MgrDashboardPage(driver);
		
		assertEquals("http://localhost:9009/html/mgr-dashboard.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testUDTableHeader() {
		loginPage.setUsername("sCurtis");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		
		this.mgrPage = new MgrDashboardPage(driver);
		
		assertEquals(mgrPage.getTableHeader(), "Reimbursements");
	}
	
	@Test
	public void testApproveReimb() throws InterruptedException {
		loginPage.setUsername("jHayhurst");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		this.mgrPage = new MgrDashboardPage(driver);
		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.className("approve"))));
		
		int oldApproveList = driver.findElements(By.className("approve")).size();
		mgrPage.approveFirst();
		Thread.sleep(5000);
		int newApproveList = driver.findElements(By.className("approve")).size();
		
		assertEquals(oldApproveList - 1, newApproveList);
		
	}

	@Test
	public void testDenyReimb() throws InterruptedException {
		loginPage.setUsername("meanBean");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		this.mgrPage = new MgrDashboardPage(driver);
		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.className("deny"))));
		
		int oldApproveList = driver.findElements(By.className("deny")).size();
		mgrPage.denyFirst();
		Thread.sleep(5000);
		int newApproveList = driver.findElements(By.className("deny")).size();
		
		assertEquals(oldApproveList - 1, newApproveList);
		
	}
	
	@Test
	public void testEmployeeDenyAccess() throws InterruptedException {
		loginPage.setUsername("userName");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		driver.get("http://localhost:9009/html/mgr-dashboard.html");
		
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
		
		Thread.sleep(2000);
		assertEquals("http://localhost:9009/html/user-dashboard.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testManagerToUserDashboard() {
		
		loginPage.setUsername("meanBean");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		this.mgrPage = new MgrDashboardPage(driver);
		
		this.mgrPage.goToUserDashboard();
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		
		assertEquals("http://localhost:9009/html/user-dashboard.html", driver.getCurrentUrl());
	}

	@Test
	public void testManagerToUserDashboardAndBack() {
		
		loginPage.setUsername("meanBean");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		this.mgrPage = new MgrDashboardPage(driver);
		
		this.mgrPage.goToUserDashboard();
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		
		driver.findElement(By.xpath("//a[text()='To Manager Dashboard']")).click();
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		
		assertEquals("http://localhost:9009/html/mgr-dashboard.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testManagerSubmitReimbursement() {
		loginPage.setUsername("meanBean");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		this.mgrPage = new MgrDashboardPage(driver);
		
		this.mgrPage.goToUserDashboard();
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		
		this.udbPage = new UserDashboardPage(driver);
		udbPage.setAmount(1000);
		udbPage.setDescription("Selenium Manager Reimbursement Test");
		udbPage.setReason("Other");
		udbPage.submit();
		
		assertEquals(driver.findElement(By.id("submit-fail")).getCssValue("visibility"), "hidden");
	}
	
	@Test
	public void testCreateNewUser() {
		loginPage.setUsername("meanBean");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		this.mgrPage = new MgrDashboardPage(driver);
		
		mgrPage.openCreateUserModal();
		
		driver.findElement(By.xpath("//input[@id='first-name']")).sendKeys("TestFirstName");
		driver.findElement(By.xpath("//input[@id='last-name']")).sendKeys("TestLastName");
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("TestUserName");
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("TestUserEmail@email.com");
		Select role = new Select(driver.findElement(By.xpath("//select[@id='role']")));
		role.selectByVisibleText("Employee");
		driver.findElement(By.xpath("//button[@id='submit']")).click();
		
		boolean modalDismissed = driver.findElement(By.xpath("//div[@id='createUser']")).isDisplayed();
		assertFalse(modalDismissed);
	}
}
