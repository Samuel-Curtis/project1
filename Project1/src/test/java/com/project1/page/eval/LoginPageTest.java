package com.project1.page.eval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.project1.page.LoginPage;

public class LoginPageTest {

	private LoginPage page;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", filePath);
		
		driver = new FirefoxDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	/* TESTS */
	
	@Test
	public void testLoginHeader() {
		assertEquals(page.getHeader(), "SIGN IN");
	}
	
	@Test
	public void testSuccessfulLogin() {
		page.setUsername("userName");
		page.setPassword("password");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		
		assertEquals("http://localhost:9009/html/user-dashboard.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testSuccessfulManagerLogin() {
		page.setUsername("sCurtis");
		page.setPassword("password");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/mgr-dashboard.html"));
		assertEquals("http://localhost:9009/html/mgr-dashboard.html", driver.getCurrentUrl());
		
	}
	
	@Test
	public void TestFailedLogin() {
		page.setUsername("random stuff");
		page.setPassword("more junk");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login-fail")));
		
		assertEquals("http://localhost:9009/html/index.html", driver.getCurrentUrl());
	}
 
}
