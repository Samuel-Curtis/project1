package com.project1.page.eval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.project1.page.LoginPage;
import com.project1.page.UserDashboardPage;

public class UserDashboardTest {

	private LoginPage loginPage;
	private UserDashboardPage udPage;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/test/resources/geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", filePath);
		
		driver = new FirefoxDriver();
		
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.loginPage = new LoginPage(driver);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	/* TESTS */
	
	@Test
	public void testUDTableHeader() {
		loginPage.setUsername("userName");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		
		this.udPage = new UserDashboardPage(driver);
		
		assertEquals(udPage.getTableHeader(), "Reimbursements");
	}
	
	@Test
	public void testUDFormHeader() {
		loginPage.setUsername("userName");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		
		this.udPage = new UserDashboardPage(driver);
		
		assertEquals(udPage.getFormHeader(), "Submit New Reimbursement");
	}
	
	@Test
	public void testSuccessfulFormSubmit() {
		loginPage.setUsername("userName");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		
		this.udPage = new UserDashboardPage(driver);
		udPage.setAmount(999);
		udPage.setDescription("Selenium Form Test");
		udPage.setReason("Other");
		udPage.submit();
		
		
		assertEquals(driver.findElement(By.id("submit-fail")).getCssValue("visibility"), "hidden");
	}

	@Test
	public void testFailedFormSubmit() {
		loginPage.setUsername("userName");
		loginPage.setPassword("password");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/user-dashboard.html"));
		
		this.udPage = new UserDashboardPage(driver);
		udPage.submit();
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
		
		assertEquals(driver.findElement(By.id("submit-fail")).getCssValue("visibility"), "visible");
	}
}
