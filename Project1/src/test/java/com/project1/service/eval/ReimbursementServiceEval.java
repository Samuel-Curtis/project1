package com.project1.service.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.dao.ReimbursementDao;
import com.project1.model.Reimbursement;
import com.project1.service.ReimbursementService;

public class ReimbursementServiceEval {

	@Mock 
	private ReimbursementDao mockRDao;
	
	private ReimbursementService testService = new ReimbursementService(mockRDao);
	private Reimbursement testReimb;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new ReimbursementService(mockRDao);
		Timestamp submitted = Timestamp.valueOf(LocalDateTime.of(2021, 01, 11, 16, 43, 07));
		Timestamp resolved = Timestamp.valueOf(LocalDateTime.of(2021, 01, 11, 11, 43, 07));
		testReimb = new Reimbursement(1000, 1000, submitted, resolved, "Description, and such", null, 100, 104, 1, 1);
		
		when(mockRDao.selectReimbById(1000)).thenReturn(testReimb);
	}
	
	@Test
	public void testSelectByIdSuccess() {
		assertEquals(testService.getReimbursementById(1000), testReimb);
	}

	@Test
	public void testSelectByIdFail() {
		assertNotEquals(testService.getReimbursementById(123), testReimb);
	}
	
	
}
