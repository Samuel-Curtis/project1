package com.project1.service.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project1.dao.UserDao;
import com.project1.model.User;
import com.project1.service.UserService;

public class UserServiceEval {

	
	@Mock
	private UserDao mockUDao;
	
	
	private UserService testService = new UserService(mockUDao);
	private User testUser;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new UserService(mockUDao);
		testUser = new User(104, "sCurtis", "password", "Samuel", "Curtis", "Curtis@email.com", 01);

		// test dependencies, not the methods themselves
		when(mockUDao.selectById(104)).thenReturn(testUser);
		when(mockUDao.selectByUsername("sCurtis")).thenReturn(testUser);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	/* *********
	 *  TESTS  *
	 * *********/
	@Test
	public void testGetUserByIdSuccess() {
		assertEquals(testService.getUser(104), testUser);
	}

	@Test
	public void testGetUserByIdFail() {
		assertNotEquals(testService.getUser(654), testUser);
	}

	@Test
	public void testGetUserByUsernameSuccess() {
		assertEquals(testService.getUser("sCurtis"), testUser);
	}

	@Test
	public void testGetUserByUsernameFail() {
		assertNotEquals(testService.getUser("not sCurtis"), testUser);
	}

	@Test
	public void testVerifyLoginSuccess() {
		User toLogin = testService.getUser("sCurtis");
		assertTrue(toLogin.getPassword() == "password");
		
	}

	@Test
	public void testVerifyLoginFail() {
		User toLogin = testService.getUser("sCurtis");
		assertFalse(toLogin.getPassword() == "notPassword");
	}
}
