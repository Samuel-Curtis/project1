package com.project1.dao.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.project1.dao.DBConnection;
import com.project1.dao.UserDao;
import com.project1.model.User;

@RunWith(MockitoJUnitRunner.class)
public class UserDaoEval {

	@Mock
	private DBConnection db;
	
	@Mock
	private Connection con;
	
	@Mock
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	UserDao uDao = new UserDao(db);
	private User testUser;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		uDao = new UserDao(db);
		when(db.getDBConnection()).thenReturn(con);
		when(con.prepareCall(any(String.class))).thenReturn(cs);
		
		testUser = new User(104, "sCurtis", "password", "Samuel", "Curtis", "Curtis@email.com", 01);
		
//		when(cs).thenReturn(cs.setInt(any(Integer.class), any(Integer.class)));
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getUserId());
		when(rs.getString(2)).thenReturn(testUser.getUserName());
		when(rs.getString(3)).thenReturn(testUser.getPassword());
		when(rs.getString(4)).thenReturn(testUser.getFirstName());
		when(rs.getString(5)).thenReturn(testUser.getLastName());
		when(rs.getString(6)).thenReturn(testUser.getEmail());
		when(rs.getInt(7)).thenReturn(testUser.getRoleId());
		when(cs.executeQuery()).thenReturn(rs);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
		
	}
	
	@Test
	public void testGetUserByIdSuccess() {
		assertEquals(uDao.selectById(104).getUserId(), testUser.getUserId());
	}
	
}
