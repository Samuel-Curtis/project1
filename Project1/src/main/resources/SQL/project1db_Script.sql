-- Create the database for project1
CREATE OR REPLACE DATABASE `project1db`;

-- The user credentials 
CREATE OR REPLACE USER 'user' IDENTIFIED BY 'myPassword1';

-- Grant permissions to user
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, TRIGGER  
ON `project1db`.* TO 'user'@'%' IDENTIFIED BY 'myPassword1' WITH GRANT OPTION;

FLUSH PRIVILEGES;

-- ---------------------------------------------------------------- --
SET sql_mode = ORACLE;
USE `project1db`;
-- ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD'; - This doesn't work for some reason, will have to figure out why..

CREATE SEQUENCE user_id_seq
	START WITH 100
	INCREMENT 1;	

CREATE SEQUENCE reimb_id_seq
	START WITH 1000
	INCREMENT 1;
	
/*********************
 * THE LOOKUP TABLES *
 *********************/

-- User Roles - Holds the roles of employees and a unique id
CREATE TABLE user_roles (
	user_role_id INTEGER,
	user_role VARCHAR2(15) NOT NULL,
	PRIMARY KEY(user_role_id)
);

-- Populate the user_roles table with required fields
INSERT INTO user_roles VALUES(01, 'Finance Manager');
INSERT INTO user_roles VALUES(05, 'Employee');

-- Reimbursement Type - Holds the categories for which employees can be reimbursed for
CREATE TABLE reimbursement_type (
	reimbursement_type_id INTEGER,
	reimbursement_type VARCHAR2(15) NOT NULL,
	PRIMARY KEY(reimbursement_type_id)
);

-- Populate the reimbursement_type table with required fields
INSERT INTO reimbursement_type VALUES(01, 'LODGING');
INSERT INTO reimbursement_type VALUES(02, 'TRAVEL');
INSERT INTO reimbursement_type VALUES(03, 'FOOD');
INSERT INTO reimbursement_type VALUES(04, 'OTHER');

-- Reimbursement Status - Holds the three potential status' of reimbursements
CREATE TABLE reimbursement_status (
	reimbursement_status_id INTEGER,
	reimbursement_status VARCHAR2(15) NOT NULL,
	PRIMARY KEY(reimbursement_status_id)
);

-- Populate the reimbursement_status table with required fields
INSERT INTO reimbursement_status VALUES(01, 'APPROVED');
INSERT INTO reimbursement_status VALUES(02, 'DENIED');
INSERT INTO reimbursement_status VALUES(05, 'PENDING');

/*********************
 *  THE MAIN TABLES  *
 *********************/

-- Users - holds information on all company employees
CREATE TABLE users (
	user_id INTEGER DEFAULT user_id_seq.NEXTVAL,
	username VARCHAR2(25) NOT NULL UNIQUE ,
	password VARCHAR2(25) NOT NULL,
	first_name VARCHAR2(25) NOT NULL,
	last_name VARCHAR2(25) NOT NULL,
	email VARCHAR2(50) NOT NULL UNIQUE ,
	role_id INTEGER  NOT NULL REFERENCES user_roles(user_role_id),
	PRIMARY KEY(user_id) 
);

CREATE TABLE reimbursement (
	reimb_id INTEGER DEFAULT reimb_id_seq.NEXTVAL,
	amount INTEGER NOT NULL,
	date_submitted TIMESTAMP NOT NULL,
	date_resolved TIMESTAMP,
	description VARCHAR2(250),
	receipt BLOB,
	author INTEGER NOT NULL REFERENCES users(user_id),
	resolver INTEGER REFERENCES users(user_id),
	status_id INTEGER DEFAULT 05 REFERENCES reimbursement_status(reimbursement_status_id),
	type_id INTEGER NOT NULL REFERENCES reimbursement_type(reimbursement_type_id),
	PRIMARY KEY(reimb_id)
);

SELECT * FROM users;
SELECT * FROM reimbursement;
-- 
-- SELECT * FROM user_roles;
-- SELECT * FROM reimbursement_status;
-- SELECT * FROM reimbursement_type;

/********************************
 *   STORED PROCEDURES - USERS  *
 ********************************/

/*******************
 *  REGISTER USER  *
 *******************/
DELIMITER //
CREATE OR REPLACE PROCEDURE register_user(username IN VARCHAR2, password IN VARCHAR2, first_name IN VARCHAR2, last_name IN VARCHAR2, email IN VARCHAR2, role_id IN INTEGER)
IS 
BEGIN 
	INSERT INTO users(users.username, users.password, users.first_name, users.last_name, users.email, users.role_id) 
	VALUES(username, password, first_name, last_name, email, role_id);
	COMMIT;
END;
//

/**********************
 *  UPDATE USER INFO  *
 **********************/
DELIMITER //
CREATE OR REPLACE PROCEDURE update_user(userID IN INTEGER, username IN VARCHAR2, password IN VARCHAR2, first_name IN VARCHAR2, last_name IN VARCHAR2, email IN VARCHAR2, role_id IN INTEGER)
IS 
BEGIN 
	UPDATE users u
	SET u.username = username, u.password = password, u.first_name = first_name, u.last_name = last_name, u.email = email, u.role_id = role_id
	WHERE u.user_id = userID;
	COMMIT;
END;
//

/*********************
 *  GET BY USERNAME  *
 *********************/
DELIMITER //
CREATE OR REPLACE PROCEDURE get_user_by_username(username IN VARCHAR2)
IS 
BEGIN 
	SELECT * FROM users u WHERE u.username = username;
END;
//

-- CALL get_user_by_username('newUsername');

/********************
 *  GET USER BY ID  *
 ********************/
DELIMITER //
CREATE OR REPLACE PROCEDURE get_user_by_id(id IN INTEGER)
IS 
BEGIN 
	SELECT * FROM users u WHERE u.user_id = id;
END;
//

CALL get_user_by_id(101);


/*****************************************
 *   STORED PROCEDURES - REIMBURSEMENTS  *
 *****************************************/

/*******************************
 *   SUBMIT NEW REIMBURSEMENT  *
 *******************************/
DELIMITER //
CREATE OR REPLACE PROCEDURE submit_reimbursement(amount IN INTEGER, submitted IN TIMESTAMP, description IN VARCHAR2, author_id IN INTEGER, type_id IN INTEGER)
IS 
BEGIN 
	INSERT INTO reimbursement(amount, date_submitted, description, author, type_id)
	VALUES(amount, submitted, description, author_id, type_id);
	COMMIT;
END;
//

/****************************
 *   RESOLVE REIMBURSEMENT  *
 ****************************/
DELIMITER //
CREATE OR REPLACE PROCEDURE resolve_reimbursement(param_id IN INTEGER, resolved IN TIMESTAMP, receipt IN BLOB, resolver_id IN INTEGER, status IN INTEGER)
IS 
BEGIN 
	UPDATE reimbursement
	SET date_resolved = resolved, receipt = receipt, resolver = resolver_id, status_id = status
	WHERE reimb_id = param_id;
	COMMIT;
END;
//

/*****************************
 *  GET REIMBURSEMENT BY ID  *
 *****************************/
DELIMITER //
CREATE OR REPLACE PROCEDURE get_reimb_by_id(id IN INTEGER)
IS 
BEGIN 
	SELECT * FROM reimbursement r WHERE r.reimb_id = id;
END;
//

/***********************************
 *  GET ALL REIMBURSEMENTS BY AUTHOR*
 ***********************************/
DELIMITER //
CREATE OR REPLACE PROCEDURE get_reimbs_by_author(author_id IN INTEGER)
IS 
BEGIN 
	SELECT * FROM reimbursement r WHERE r.author = author_id;
END;
//

/****************************************
 *  GET ALL REIMBURSEMENTS BY RESOLVER  *
 ****************************************/
DELIMITER //
CREATE OR REPLACE PROCEDURE get_reimbs_by_resolver(resolver_id IN INTEGER)
IS 
BEGIN 
	SELECT * FROM reimbursement r WHERE r.resolver = resolver_id;
END;
//

/**************************************
 *  GET ALL REIMBURSEMENTS BY STATUS  *
 **************************************/
DELIMITER //
CREATE OR REPLACE PROCEDURE get_reimbs_by_status(status_id IN INTEGER)
IS 
BEGIN 
	SELECT * FROM reimbursement r WHERE r.status_id = status_id;
END;
//




