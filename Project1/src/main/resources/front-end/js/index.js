/**
 * 
 */
window.onload = () => {
	document.getElementById('submit').addEventListener('click', doSignIn);
}
function doSignIn() {
	let userForm = new FormData();

	userForm.append("username", document.getElementById('username').value);
	userForm.append("password", document.getElementById('password').value);

	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = () => {

		if(xhttp.readyState == 4) {
			
			if(xhttp.status == 222) {
				// Redirecting through JS because Javalin wants to be silly
				let user = JSON.parse(xhttp.responseText);
				if(user.roleId == 1) {
					window.location.href = `/html/mgr-dashboard.html`;
				} else {
					window.location.href = `/html/user-dashboard.html`;
				}
			}
			else {
				document.getElementById('login-fail').style.visibility = 'visible';
			}
		}
	}

	xhttp.open("POST", "http://localhost:9009/sign-in");
	xhttp.send(userForm);
}