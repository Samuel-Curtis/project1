/*****************
 * Get User Info *
 *****************/

function getSessionUser() {
    
  let xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = () => {

      if(xhttp.readyState == 4) {
        if(xhttp.status == 200) {
          let user = JSON.parse(xhttp.responseText);
          console.log('getSessionUser called and returned: ' + user);
          loadUserContent(user);
        }
        else if(xhttp.status == 401) {
          console.log("Unauthorized, user is not logged in.");
          window.location.href = "/html/index.html";
        }
      }

  }

  xhttp.open("GET", "http://localhost:9009/getSessionUser");
  xhttp.send();
}

/************************
 * Load Content To Page *
 ************************/

function loadUserContent(sessUser) {
    
  document.getElementById('user-name').innerHTML = `<h3>${sessUser.lastName}, ${sessUser.firstName}</h3>`;

  if(window.location.href.includes("/html/mgr-dashboard.html")) {
    if(sessUser.roleId == 5) {
      alert('Only Managers are allowed to access the manager dashboard. You will be redirected to the User Dashboard.');
      window.location.href = "/html/user-dashboard.html";
    }
  } 
  
  if(window.location.href.includes("/html/user-dashboard.html")) {

    if(sessUser.roleId == 5) {
      document.getElementById('toMgrDB').style.visibility = "hidden";
    } else {
      document.getElementById('toMgrDB').style.visibility = "visible";
    }

  }
}

/**************
 *  Sign Out  *
 **************/
function signOut() {

  let xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = () => {
      if(xhttp.readyState == 4 && xhttp.status == 234) {
          window.location.href = "/html/index.html";
      }
  }

  xhttp.open("GET", "http://localhost:9009/sign-out")
  xhttp.send();
}

/***************************
 * Reimb Utility Functions *
 ***************************/

function getTypeById(typeId) {

  switch(typeId) {
      case 1:
          return 'LODGING';
      case 2:
          return 'TRAVEL';
      case 3:
          return 'FOOD';
      case 4:
          return 'OTHER';
  }

}

function getStatusById(statusId) {

  switch(statusId) {
      case 1:
          return 'APPROVED';
      case 2:
          return 'DENIED';
      case 5:
          return 'PENDING';
  }

}