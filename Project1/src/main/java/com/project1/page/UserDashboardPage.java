package com.project1.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class UserDashboardPage {

	private WebDriver driver;
	
	private WebElement tableHeader;
	private WebElement formHeader;
	private WebElement amount;
	private Select reason;
	private WebElement description;
	private WebElement submitButton;
	
	public UserDashboardPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.tableHeader = driver.findElement(By.tagName("h1"));
		this.formHeader = driver.findElement(By.tagName("h2"));
		this.amount = driver.findElement(By.id("new-amount"));
		this.reason = new Select(driver.findElement(By.id("inputReason")));
		this.description = driver.findElement(By.id("description"));
		this.submitButton = driver.findElement(By.id("submit"));
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9009/html/user-dashboard.html");
	}

	public void setAmount(int amount) {
		this.amount.clear();
		this.amount.sendKeys(String.valueOf(amount));
	}
	
	public String getAmount() {
		return this.amount.getAttribute("value");
	}
	
	public void setReason(String select) {
		this.reason.selectByVisibleText(select);
	}
	
	public void setDescription(String text) {
		this.description.clear();
		this.description.sendKeys(text);
	}
	
	public String getDescription() {
		return this.description.getAttribute("value");
	}
	
	public String getTableHeader() {
		return this.tableHeader.getText();
	}
	
	public String getFormHeader() {
		return this.formHeader.getText();
	}
	
	public void submit() {
		this.submitButton.click();
	}
}
