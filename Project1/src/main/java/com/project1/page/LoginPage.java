package com.project1.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

	private WebDriver driver;
	
	private WebElement header;
	private WebElement username;
	private WebElement password;
	private WebElement submitButton;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.header = driver.findElement(By.tagName("h1"));
		this.username = driver.findElement(By.id("username"));
		this.password = driver.findElement(By.id("password"));
		this.submitButton = driver.findElement(By.id("submit"));
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9009/html/index.html");
	}
	
	public void setUsername(String name) {
		this.username.clear();
		this.username.sendKeys(name);
	}
	
	public String getUsername() {
		return this.username.getAttribute("value");
	}
	
	public void setPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}
	
	public String getPassword() {
		return this.password.getAttribute("value");
	}
	
	public String getHeader() {
		return this.header.getText();
	}
	
	public void submit() {
		this.submitButton.click();
	}
}
