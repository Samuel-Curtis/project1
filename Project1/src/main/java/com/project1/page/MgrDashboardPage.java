package com.project1.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MgrDashboardPage {

	private WebDriver driver;
	
	private WebElement tableHeader;
	private WebElement toUserDashboard;
	private WebElement createUser;
	
	private List<WebElement> approveButton;
	private List<WebElement> denyButton;
	
	public MgrDashboardPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.tableHeader = driver.findElement(By.tagName("h1"));
		this.toUserDashboard = driver.findElement(By.id("userDashboard"));
		this.createUser = driver.findElement(By.id("createUserButton"));
		
		this.approveButton = driver.findElements(By.className("approve"));
		this.denyButton = driver.findElements(By.className("deny"));
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9009/html/mgr-dashboard.html");
	}
	
	public String getTableHeader() {
		return this.tableHeader.getText();
	}
	
	public void approveFirst() {
		this.approveButton.get(0).click();
	}
	
	public void denyFirst() {
		this.denyButton.get(0).click();
	}
	
	public void goToUserDashboard() {
		this.toUserDashboard.click();
	}
	
	public void openCreateUserModal() {
		this.createUser.click();
	}
}
