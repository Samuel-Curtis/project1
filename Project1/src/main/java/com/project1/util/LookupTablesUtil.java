package com.project1.util;

import com.project1.model.ReimbursementStatus;
import com.project1.model.ReimbursementType;
import com.project1.model.UserRole;

public class LookupTablesUtil {

	/* *************
	 *  USER ROLE  *
	 * *************/
	final UserRole employee = new UserRole(05, "Employee");
	final UserRole financeManager = new UserRole(01, "Finance Manager");
	
	public UserRole getRoleById(int id) {

		if(id == 01) {
			return financeManager;
		} 
		else if(id == 05) {
			return employee;
		}
		else {
			return null;
		}
		
	}
	
	/* **********************
	 *  REIMBURSEMENT TYPE  *
	 * **********************/

	final ReimbursementType LODGING = new ReimbursementType(01, "LODGING");
	final ReimbursementType TRAVEL = new ReimbursementType(02, "TRAVEL");
	final ReimbursementType FOOD = new ReimbursementType(03, "FOOD");
	final ReimbursementType OTHER = new ReimbursementType(04, "OTHER");
	
	public ReimbursementType getReimTypeById(int id) {
		
		switch (id) {
		case 01:
			return LODGING;
		case 02:
			return TRAVEL;
		case 03:
			return FOOD;
		case 04:
			return OTHER;
		default:
			return null;
		}
	}
	
	/* ************************
	 *  REIMBURSEMENT STATUS  *
	 * ************************/
	final ReimbursementStatus PENDING = new ReimbursementStatus(05, "PENDING");
	final ReimbursementStatus APPROVED = new ReimbursementStatus(01, "APPROVED");
	final ReimbursementStatus DENIED = new ReimbursementStatus(02, "DENIED");
	
public ReimbursementStatus getReimStatusById(int id) {
		
		switch (id) {
		case 01:
			return APPROVED;
		case 02:
			return DENIED;
		case 05:
			return PENDING;
		default:
			return null;
		}
	}

}
