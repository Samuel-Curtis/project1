package com.project1.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.project1.model.User;
import org.apache.log4j.Logger;

public class UserDao {
	
	public final static Logger log = Logger.getLogger(UserDao.class);

	/* ************
	 *   FIELDS   *
	 * ************/
	
	private DBConnection db;
	
	/* ****************
	 *   CONSTRUCTORS *
	 * ****************/
	
	/**
	 * Creates a new, empty UserDao Object
	 */
	public UserDao() {
		super();
	}

	/**
	 * Creates a new parameterized UserDao Object
	 * @param sUtil
	 */
	public UserDao(DBConnection db) {
		super();
		this.db = db;
	}
	
	/* ************
	 *   METHODS  *
	 * ************/
	
	/**
	 * Adds a new user to the database 
	 * 
	 * @param user A User object representation of the user to be added 
	 */
	public void registerUser(User user) {
		
		try(Connection con = db.getDBConnection()) {
			
		String sql = "{ CALL register_user(?, ?, ?, ?, ?, ?) }";
		
			CallableStatement cs = con.prepareCall(sql);
			cs.setString(1, user.getUserName());
			cs.setString(2, user.getPassword());
			cs.setString(3, user.getFirstName());
			cs.setString(4, user.getLastName());
			cs.setString(5, user.getEmail());
			cs.setInt(6, user.getRoleId());
		
			cs.execute();
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
		log.info("New User was registered: " + selectByUsername(user.getUserName()).toString());
	}
	
	public User selectByUsername(String username) {
		User user = null;
		
		try(Connection con = db.getDBConnection()) {
			
		
			String sql = "CALL get_user_by_username(?)";
			CallableStatement cs = con.prepareCall(sql);
			cs.setString(1, username);
		
			ResultSet rs = cs.executeQuery();
		
			if(rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
							rs.getString(5), rs.getString(6), rs.getInt(7));
			}
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

	public User selectById(int id) {
		User user = null;
		
		try(Connection con = db.getDBConnection()) {
			
			String sql = "CALL get_user_by_id(?)";
		
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, id);
		
			ResultSet rs = cs.executeQuery();
		
			if(rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
								rs.getString(5), rs.getString(6), rs.getInt(7));
			}
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
	
		return user;
	}
	
	public void updateUser(User user) {
		
		try(Connection con = db.getDBConnection()) {
			
			String sql = "{ CALL update_user(?, ?, ?, ?, ?, ?, ?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, user.getUserId());
			cs.setString(2, user.getUserName());
			cs.setString(3, user.getPassword());
			cs.setString(4, user.getFirstName());
			cs.setString(5, user.getLastName());
			cs.setString(6, user.getEmail());
			cs.setInt(7, user.getRoleId());
			
			cs.execute();
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		log.info("User was updated: " + selectByUsername(user.getUserName()).toString());
		
	} 
	
}
