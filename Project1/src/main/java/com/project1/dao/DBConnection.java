package com.project1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	private String url = "jdbc:mariadb://database-1.ceodkn20iczw.us-east-2.rds.amazonaws.com:3306/project1db";
	private String user = "user";
	private String password = "myPassword1";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}
}
