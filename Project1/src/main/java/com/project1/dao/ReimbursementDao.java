package com.project1.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.project1.model.Reimbursement;

public class ReimbursementDao {

	public final static Logger log = Logger.getLogger(ReimbursementDao.class);
	
	/* ************
	 *   FIELDS   *
	 * ************/
	private DBConnection db;
	
	/* ****************
	 *   CONSTRUCTORS *
	 * ****************/
	
	public ReimbursementDao() {
		super();
	}

	public ReimbursementDao(DBConnection db) {
		super();
		this.db = db;
	}
	
	/* ************
	 *   METHODS  *
	 * ************/
	
	public void submitReimbursement(Reimbursement reimb) {
	
		try(Connection con = db.getDBConnection()) {
			
			String sql = "{ CALL submit_reimbursement(?, ?, ?, ?, ?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, reimb.getAmount());
			cs.setTimestamp(2, reimb.getSubmitted());
			cs.setString(3, reimb.getDescription());
			cs.setInt(4, reimb.getAuthorId());
			cs.setInt(5, reimb.getTypeId());
			
			cs.execute();
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		log.info("New Reimbursement Was Submitted: " + reimb.toString());
	}

	public void resolveReimbursement(Reimbursement reimb) {
		
		try(Connection con = db.getDBConnection()) {
			
			String sql = "{ CALL resolve_reimbursement(?, ?, ?, ?, ?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, reimb.getReimbId());
			cs.setTimestamp(2, reimb.getResolved());
			cs.setBlob(3, reimb.getReceipt());
			cs.setInt(4, reimb.getResolverId());
			cs.setInt(5, reimb.getStatusId());
			
			cs.execute();
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		log.info("Reimbursement Was Resolved: " + reimb.toString());
	}
	
	public Reimbursement selectReimbById(int id) {
		Reimbursement reimb = null;
		
		try(Connection con = db.getDBConnection()) {
			
			String sql = "{ CALL get_reimb_by_id(?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, id);
			
			ResultSet rs = cs.executeQuery();
			
			if(rs.next()) {
				reimb = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), 
						rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		
		return reimb;
	}
	
	public List<Reimbursement> selectAllByAuthor(int userId) {
		List<Reimbursement> reimbs = new ArrayList<>();
		
		try(Connection con = db.getDBConnection()) {
			
			String sql = "{ CALL get_reimbs_by_author(?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, userId);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				reimbs.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), 
						rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}

		return reimbs;
	}

	public List<Reimbursement> selectAllByResolver(int userId) {
		List<Reimbursement> reimbs = new ArrayList<>();
		
		try(Connection con = db.getDBConnection()) {
			
			String sql = "{ CALL get_reimbs_by_resolver(?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, userId);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				reimbs.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), 
						rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimbs;
	}

	public List<Reimbursement> selectAllBystatus(int statusId) {
		List<Reimbursement> reimbs = new ArrayList<>();
		
		try(Connection con = db.getDBConnection()) {
			
			String sql = "{ CALL get_reimbs_by_status(?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, statusId);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				reimbs.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), 
						rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimbs;
	}
	
}
