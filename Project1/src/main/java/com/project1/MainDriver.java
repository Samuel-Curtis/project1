package com.project1;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.project1.controller.ReimbursementController;
import com.project1.controller.UserController;
import com.project1.dao.DBConnection;
import com.project1.dao.ReimbursementDao;
import com.project1.dao.UserDao;
import com.project1.model.Reimbursement;
import com.project1.model.User;
import com.project1.service.ReimbursementService;
import com.project1.service.UserService;

import io.javalin.Javalin;

public class MainDriver {
	
	public static void main(String[] args) {
		
		DBConnection db = new DBConnection();
		
		UserDao uDao = new UserDao(db);
		UserService uServ = new UserService(uDao);
		UserController uCon = new UserController(uServ);
		
		ReimbursementDao rDao = new ReimbursementDao(db);
		ReimbursementService rServ = new ReimbursementService(rDao);
		ReimbursementController rCon = new ReimbursementController(rServ);
		
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/front-end");
		});
		
		app.start(9009);
		
		/* *******************
		 *  User Controller  *
		 * *******************/
		app.get("/", uCon.HOME);
		
		app.post("/sign-in", uCon.SIGN_IN);
		
		app.get("/sign-out", uCon.SIGN_OUT);
		
		app.get("/getSessionUser", uCon.GET_SESSION_USER);
		
		app.post("/createUser", uCon.CREATE_NEW_USER);
		
		/* ****************************
		 *  Reimbursement Controller  *
		 * ****************************/
		
		app.get("/reimbursements", rCon.GET_REIMBS_BY_AUTHOR);
		
		app.post("/reimbursements/submit", rCon.SUBMIT_REIMBURSEMENT);
		
		app.post("/reimbursements/resolve/:reimbId/approve", rCon.APPROVE_REIMBURSEMENT);
		
		app.post("/reimbursements/resolve/:reimbId/deny", rCon.DENY_REIMBURSEMENT);
		
		app.get("reimbursements/status/:statusId", rCon.GET_REIMBS_BY_STATUS);
		
		
		
//		loadInitData();

		
	}
	
	public static void loadInitData() {
		
		DBConnection db = new DBConnection();
		
		UserDao uDao = new UserDao(db);
		UserService uServ = new UserService(uDao);
		
		ReimbursementDao rDao = new ReimbursementDao(db);
		ReimbursementService rServ = new ReimbursementService(rDao);
		
		// Employees
		User emp1 = new User("userName", "password", "firstName", "lastName", "email@email.com", 05);
		User emp2 = new User("newUserName", "password", "newFirstName", "newLastName", "newEmail@email.com", 05);
		User emp3 = new User("AlexHam", "password", "Alexander", "Hamilton", "HamiltonAlex@aol.com", 05);
		User emp4 = new User("BurrSir", "password", "Aaron", "Burr", "ArronBurrSir@gmail.com", 05);
		
		// Managers
		User mgr1 = new User("sCurtis", "password", "Samuel", "Curtis", "Curtis@email.com", 01);
		User mgr2 = new User("jHayhurst", "password", "Jacob", "Hayhurst", "Hayhurst@email.com", 01);
		User mgr3 = new User("meanBean", "password", "Mean", "Manager", "MrMeanBean@email.com", 01);
		
		ArrayList<User> users = new ArrayList<>();
		users.add(emp1);
		users.add(emp2);
		users.add(emp3);
		users.add(emp4);
		users.add(mgr1);
		users.add(mgr2);
		users.add(mgr3);
		
		for(User user : users) {
			uServ.registerUser(user);
		}
		
		// Submit new Reimbs
		Timestamp time = Timestamp.valueOf(LocalDateTime.now());
		Reimbursement reimb1 = new Reimbursement(1000, time, "Description, and such", 100, 01);
		Reimbursement reimb2 = new Reimbursement(650, time, "Another description, I guess", 100, 02);
		Reimbursement reimb3 = new Reimbursement(750, time, "Gimme dat money", 101, 02);
		Reimbursement reimb4 = new Reimbursement(345, time, "I got some grub!", 102, 03);
		Reimbursement reimb5 = new Reimbursement(722, time, "Description, and such", 102, 04);
		Reimbursement reimb6 = new Reimbursement(892, time, "This shouldn't be resolved!", 104, 04);
		
		ArrayList<Reimbursement> reimbs = new ArrayList<>();
		reimbs.add(reimb1);
		reimbs.add(reimb2);
		reimbs.add(reimb3);
		reimbs.add(reimb4);
		reimbs.add(reimb5);
		reimbs.add(reimb6);
		
		for(Reimbursement reimb : reimbs) {
			rServ.submitNewReimbursement(reimb);
		}
		
//		// 01: approved  	02: denied
		List<Reimbursement> resReimbs = rServ.getAllReimbursementsByStatus(05);
		Timestamp resolved = Timestamp.valueOf(LocalDateTime.now());
		
		resReimbs.get(1).setResolved(resolved);
		resReimbs.get(1).setResolverId(104);
		resReimbs.get(1).setStatusId(02);
		
		resReimbs.get(2).setResolved(resolved);
		resReimbs.get(2).setResolverId(104);
		resReimbs.get(2).setStatusId(02);
		
		resReimbs.get(4).setResolved(resolved);
		resReimbs.get(4).setResolverId(106);
		resReimbs.get(4).setStatusId(01);
		
		rServ.resolveReimbursement(resReimbs.get(1));
		rServ.resolveReimbursement(resReimbs.get(2));
		rServ.resolveReimbursement(resReimbs.get(4));
		
			
		
		
	}
	

}
