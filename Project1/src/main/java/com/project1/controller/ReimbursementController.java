package com.project1.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import com.project1.model.Reimbursement;
import com.project1.model.User;
import com.project1.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {

	private ReimbursementService rServ;
	
	public ReimbursementController() {
		super();
	}

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}
	
	public Handler GET_REIMBS_BY_AUTHOR = (ctx) -> {
		User sessUser = (User) ctx.sessionAttribute("user");
		
		List<Reimbursement> reimbs = rServ.getReimbursementsByAuthor(sessUser);
		
		ctx.json(reimbs);
	};

	public Handler GET_REIMBS_BY_STATUS = (ctx) -> {
		
		int status = Integer.parseInt(ctx.pathParam("statusId"));
		List<Reimbursement> reimbs = rServ.getAllReimbursementsByStatus(status);
		
		ctx.json(reimbs);
	};
	
	public Handler SUBMIT_REIMBURSEMENT = (ctx) -> {
		User sessUser = (User) ctx.sessionAttribute("user");
		
		int amount = 0;
		try {
			amount = Integer.parseInt(ctx.formParam("amount"));
			
		} catch(NumberFormatException e) {
			ctx.status(440);
			return;
		}
		if(amount <= 0) {
			ctx.status(445);
			return;
		}
		
		Timestamp now = Timestamp.valueOf(LocalDateTime.now());
		String description = ctx.formParam("description");
		int typeId = Integer.parseInt(ctx.formParam("typeId"));
		
		if(typeId == 0) {
			ctx.status(446);
			return;
		}
		
		
		Reimbursement toSubmit = new Reimbursement(amount, now, description, sessUser.getUserId(), typeId);
		rServ.submitNewReimbursement(toSubmit);
		
		ctx.status(222);
	
	};
	
	public Handler APPROVE_REIMBURSEMENT = (ctx) -> {
		User sessUser = (User) ctx.sessionAttribute("user");
		
		Reimbursement toApprove = rServ.getReimbursementById(Integer.parseInt(ctx.pathParam("reimbId")));
		if(toApprove.getAuthorId() == sessUser.getUserId()) {
			ctx.status(402);
			return;
		}
		toApprove.setResolved(Timestamp.valueOf(LocalDateTime.now()));
		toApprove.setResolverId(sessUser.getUserId());
		toApprove.setStatusId(1);
		
		rServ.resolveReimbursement(toApprove);
		ctx.status(222);
	};

	public Handler DENY_REIMBURSEMENT = (ctx) -> {
		User sessUser = (User) ctx.sessionAttribute("user");
		
		Reimbursement toDeny = rServ.getReimbursementById(Integer.parseInt(ctx.pathParam("reimbId")));
		if(toDeny.getAuthorId() == sessUser.getUserId()) {
			ctx.status(402);
			return;
		}
		toDeny.setResolved(Timestamp.valueOf(LocalDateTime.now()));
		toDeny.setResolverId(sessUser.getUserId());
		toDeny.setStatusId(2);
		
		rServ.resolveReimbursement(toDeny);
		ctx.status(222);
	};
}
