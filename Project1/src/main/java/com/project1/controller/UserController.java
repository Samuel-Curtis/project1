package com.project1.controller;

import com.project1.model.User;
import com.project1.service.UserService;


import io.javalin.http.Handler;

public class UserController {

	private UserService uServ;
	
	public UserController() {
		super();
	}

	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	
	public Handler HOME = (ctx) -> {
		ctx.redirect("/html/index.html");
	};
	
	public Handler SIGN_IN = (ctx) -> {
		
		if(uServ.verifyLogin(ctx.formParam("username"), ctx.formParam("password"))) {
			
			User user = uServ.getUser(ctx.formParam("username"));
			
			ctx.sessionAttribute("user", user);
			ctx.status(222);
			ctx.json(user);
	
		} else {
			ctx.status(400);
		}

	};

	public Handler SIGN_OUT = (ctx) -> {
			
			ctx.sessionAttribute("user", null);
			ctx.status(234);
		
	};
	
	public Handler GET_SESSION_USER = (ctx) -> {
		User sessUser = (User) ctx.sessionAttribute("user");
		
		if(sessUser != null) {
			ctx.json(sessUser);
		} else {
			System.out.println("No Session User, redirect to login");
			ctx.status(401);
		}
	};
	
	public Handler CREATE_NEW_USER = (ctx) -> {
		
		String userName = ctx.formParam("userName");
		String password = "password";
		String firstName = ctx.formParam("firstName");
		String lastName = ctx.formParam("lastName");
		String email = ctx.formParam("email");
		int roleId = Integer.parseInt(ctx.formParam("role"));
		
		User newUser = new User(userName, password, firstName, lastName, email, roleId);
		
		
		
		uServ.registerUser(newUser);
		ctx.status(200);
		return;
		
	};
	
}
