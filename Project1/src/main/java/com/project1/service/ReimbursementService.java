package com.project1.service;

import java.util.List;

import com.project1.dao.ReimbursementDao;
import com.project1.model.Reimbursement;
import com.project1.model.User;

public class ReimbursementService {

	private ReimbursementDao rDao;
	
	public ReimbursementService() {
		super();
	}

	public ReimbursementService(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}
	
	// Methods
	
	// CREATE
	public void submitNewReimbursement(Reimbursement reimb) {
		try {	
			rDao.submitReimbursement(reimb);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// UPDATE
	public void resolveReimbursement(Reimbursement reimb) {
		try {
			rDao.resolveReimbursement(reimb);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// READ
	// Single
	public Reimbursement getReimbursementById(int id) {
		Reimbursement reimb = rDao.selectReimbById(id);

		return reimb;
	}
	
	// Grouped
	public List<Reimbursement> getReimbursementsByAuthor(User user) {
		List<Reimbursement> reimbs = rDao.selectAllByAuthor(user.getUserId());
		
		return reimbs;
	}

	public List<Reimbursement> getReimbursementsByResolver(User user) {
		List<Reimbursement> reimbs = rDao.selectAllByResolver(user.getUserId());
	
		return reimbs;
	}

	public List<Reimbursement> getAllReimbursementsByStatus(int status) { // May change to use the ReimbStatus object, tbd..
		List<Reimbursement> reimbs = rDao.selectAllBystatus(status);
	
		return reimbs;
	}
	
	
}
