package com.project1.service;

import com.project1.dao.UserDao;
import com.project1.model.User;

public class UserService {

	private UserDao uDao;
	
	public UserService() {
		super();
	}

	public UserService(UserDao uDao) {
		super();
		this.uDao = uDao;
	}
	
	// Methods
	
	// CREATE 
	public void registerUser(User user) {
		try {
			uDao.registerUser(user);			
		} catch(Exception e){	
			e.printStackTrace();
		}
	}
	
	// READ
	public User getUser(String username) {
		User user = uDao.selectByUsername(username);
		
		return user;
	}
	
	public User getUser(int id) {
		User user = uDao.selectById(id);
		
		return user;
	}
	
	public boolean verifyLogin(String username, String password) {
		User user = getUser(username);
		boolean verify = false;
		
		if(user == null) {
			return verify;
		}
		
		if(user.getPassword().equals(password)) {
			verify = true;
		}
		
		return verify;
	}
	
	// Update
	public void updateUserInfo(User user) {
		uDao.updateUser(user);
	}
}
