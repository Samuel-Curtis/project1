package com.project1.model;

public class UserRole {

	/* ************
	 *   FIELDS   *
	 * ************/
	
	private int userRoleId;
	
	private String role;
	
	/* *****************
	 *   CONSTRUCTORS  *
	 * *****************/
	
	/**
	 * Creates a new, empty UserRole Object
	 */
	public UserRole() {
		super();
	}
	
	/**
	 * Constructs a UserRole Object 
	 * 
	 * @param userRoleId
	 * @param role
	 */
	public UserRole(int userRoleId, String role) {
		super();
		this.userRoleId = userRoleId;
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserRole [userRoleId=" + userRoleId + ", role=" + role + "]";
	}

	public int getUserRoleId() {
		return userRoleId;
	}
	
}
