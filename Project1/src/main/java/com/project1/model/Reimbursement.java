package com.project1.model;

import java.sql.Blob;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;


public class Reimbursement {

	/* ************
	 *   FIELDS   *
	 * ************/
	
	private int reimbId;
	private int amount;
	private String description;
	private Blob receipt;
	private int authorId;
	private int resolverId;
	private int statusId;
	private int typeId;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Timestamp submitted;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Timestamp resolved;
	
	/* *****************
	 *   CONSTRUCTORS  *
	 * *****************/
	
	/**
	 * Constructs a new, empty Reimbursement Object
	 */
	public Reimbursement() {
		super();
	}

	/**
	 * Constructs a new Reimbursement Object. This particular constructor represents 
	 * what would be expected when a user first submits a Reimbursement request.
	 * 
	 * @param amount
	 * @param submitted
	 * @param description
	 * @param author
	 * @param type
	 */
	public Reimbursement(int amount, Timestamp submitted, String description, int authorId, int typeId) {
		super();
		this.amount = amount;
		this.submitted = submitted;
		this.description = description;
		this.authorId = authorId;
		this.typeId = typeId;
	}

	/**
	 * Constructs a new parameterized Reimbursement Object
	 * 
	 * @param reimbId
	 * @param amount
	 * @param submitted
	 * @param resolved
	 * @param description
	 * @param receipt
	 * @param author
	 * @param resolver
	 * @param status
	 * @param type
	 */
	public Reimbursement(int reimbId, int amount, Timestamp submitted, Timestamp resolved, String description,
			Blob receipt, int authorId, int resolverId, int statusId, int typeId) {
		super();
		this.reimbId = reimbId;
		this.amount = amount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.description = description;
		this.receipt = receipt;
		this.authorId = authorId;
		this.resolverId = resolverId;
		this.statusId = statusId;
		this.typeId = typeId;
	}

	/* *************
	 *   ToSTRING  *
	 * *************/
	
	@Override
	public String toString() {
		return "Reimbursement [reimbId=" + reimbId + ", amount=" + amount + ", submitted=" + submitted + ", resolved="
				+ resolved + ", description=" + description + ", receipt=" + receipt + ", authorId=" + authorId
				+ ", resolverId=" + resolverId + ", statusId=" + statusId + ", typeId=" + typeId + "]";
	}

	/* ************************
	 *   GETTERS AND SETTERS  *
	 * ************************/
	
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Timestamp getSubmitted() {
		return submitted;
	}

	public void setSubmitted(Timestamp submitted) {
		this.submitted = submitted;
	}

	public Timestamp getResolved() {
		return resolved;
	}

	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Blob getReceipt() {
		return receipt;
	}

	public void setReceipt(Blob receipt) {
		this.receipt = receipt;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public int getResolverId() {
		return resolverId;
	}

	public void setResolverId(int resolverId) {
		this.resolverId = resolverId;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public int getReimbId() {
		return reimbId;
	}
	
}
