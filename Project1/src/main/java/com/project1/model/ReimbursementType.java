package com.project1.model;

public class ReimbursementType {

	/* ************
	 *   FIELDS   *
	 * ************/

	private int reimbTypeId;

	private String reimbType;
	
	/* ****************
	 *   CONSTRUCTOR  *
	 * ****************/
	
	public ReimbursementType() {
		super();
	}
	
	/**
	 * Constructs a ReimbursementType Object
	 * 
	 * @param reimbId
	 * @param reimbType
	 */
	public ReimbursementType(int reimbTypeId, String reimbType) {
		super();
		this.reimbTypeId = reimbTypeId;
		this.reimbType = reimbType;
	}

	public int getReimbTypeId() {
		return reimbTypeId;
	}
	
	
}
