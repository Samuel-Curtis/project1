package com.project1.model;

public class ReimbursementStatus {

	/* ************
	 *   FIELDS   *
	 * ************/

	private int reimbStatusId;
	
	private String reimbStatus;

	/* ****************
	 *   CONSTRUCTOR  *
	 * ****************/
	
	public ReimbursementStatus() {
		super();
	}
	
	/**
	 * Constructs a ReimbursementStatus Object
	 * 
	 * @param reimbId
	 * @param reimbStatus
	 */
	public ReimbursementStatus(int reimbStatusId, String reimbStatus) {
		super();
		this.reimbStatusId = reimbStatusId;
		this.reimbStatus = reimbStatus;
	}

	public int getReimbStatusId() {
		return reimbStatusId;
	}
	
	
}