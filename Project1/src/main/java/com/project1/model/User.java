package com.project1.model;

public class User {

	/* ************
	 *   FIELDS   *
	 * ************/

	private int userId;
	private String userName;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private int roleId;
	
	
	/* ****************
	 *   CONSTRUCTORS *
	 * ****************/ 
	
	/**
	 * Constructs a new, empty User Object
	 */
	public User() {
		super();
	}

	public User(int id) {
		super();
		this.userId = id;
	}
	/**
	 * Constructs a new User Object without an ID field
	 * 
	 * @param userName
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param userRole
	 */
	public User(String userName, String password, String firstName, String lastName, String email, int roleId) {
		super();
		this.userName = userName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.roleId = roleId;
	}

	/**
	 * Constructs a new User Object with an ID field
	 * 
	 * @param userId 
	 * @param userName
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param userRole
	 */
	public User(int userId, String userName, String password, String firstName, String lastName, String email, int roleId) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.roleId = roleId;
	}

	/* *************
	 *   ToSTRING  *
	 * *************/
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", password=" + password + ", firstName="
				+ firstName + ", lastName=" + lastName + ", email=" + email + ", roleId=" + roleId + "]";
	}

	/* ************************
	 *   GETTERS AND SETTERS  *
	 * ************************/
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getUserId() {
		return userId;
	}
	
}
