# Project 1: Expense Reimbursement System (ERS) - JWA v3.1

## Project Description

The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used

* Java, Javalin, Oracle SQL, JDBC
* HTML, CSS, JavaScript
* BootStrap
* AJAX
* JUnit, Mockito, Selenium

## Features

List of features ready and TODOs for future development
* Login / Logout functionality
* As a user you can view all past reimbursements and submit new ones
* As a manager, you can approve or deny reimbursements, as well as visit your own user dashboard to submit reimbursements

To-do list:
* Add functionality to create new users from the manager dashboard
* basic code cleanup

## Getting Started
   
1. git clone https://gitlab.com/Samuel-Curtis/project1.git
2. Run the SQL script located in src/main/resources/SQL in a mariaDB environment
3. Open the project using Spring Tool Suite
4. Update the url in DBConnection located in src/main/java/com/project1/dao to your database
5. In order to start with dummy data, uncomment the loadInitValues() method call in MainDriver located in src/main/java/com/project1
6. Run the application

## Usage

1. Once the application starts you should be able to go to http://localhost:9009 to visit the login page
2. Login using the username and password of a user (dummy data located in the loadInitValues() method)
3. View past reimbursements or submit a new one using the form on the User Dashboard page
4. Click Log Out when finished
5. Log in as a manager to view all reimbursements, click approve or deny. 
6. To submit a reimbursement as a manager, click the "User Dashboard" at the at the top and repeat step 3
7. Afterwards, click "Manager Dashboard" at the top to visit the Manager Dashboard again
8. Note that you can see your own reimbursement, but you cannot approve or deny it, go ahead and try!
